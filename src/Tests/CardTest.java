package Tests;

import Cards.*;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CardTest {

    @Test
    void coloredCanPlayAfterColored() {
        ColoredCard before = new NumberedCard(Color.RED, CardValue.EIGHT);
        ColoredCard validOption = new PickUpTwo(Color.RED);
        ColoredCard differentColorSameNumber = new NumberedCard(Color.BLUE, CardValue.EIGHT);
        ColoredCard invalidOption = new NumberedCard(Color.YELLOW, CardValue.ZERO);

        assertTrue(validOption.canPlay(before));
        assertTrue(differentColorSameNumber.canPlay(before));
        assertFalse(invalidOption.canPlay(before));
    }

    @Test
    void coloredCanPlayAfterNeutral() {
        NeutralCard before = new WildCard();
        ColoredCard colored = new NumberedCard(Color.BLUE, CardValue.EIGHT);

        // If no color is set it should return false.
        assertFalse(colored.canPlay(before));

        before.pickColor(Color.BLUE);
        assertTrue(colored.canPlay(before));
    }

    @Test
    void neutralCanPlay() {
        NeutralCard beforeNeutral = new WildCard();
        ColoredCard beforeColored = new ReverseCard(Color.RED);
        NeutralCard neutral = new PickUpFour();

        assertTrue(neutral.canPlay(beforeNeutral));
        assertTrue(neutral.canPlay(beforeColored));
    }


}