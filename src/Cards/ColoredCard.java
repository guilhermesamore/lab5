package Cards;

public abstract  class ColoredCard implements Card {

    private final Color color;
    private final CardValue value;

    ColoredCard(Color color, CardValue value) {
        this.color = color;
        this.value = value;
    }


    @Override
    public boolean canPlay(Card last_played) {
        if (last_played instanceof ColoredCard) return this.canPlay((ColoredCard) last_played);
        if (last_played instanceof NeutralCard) return this.canPlay((NeutralCard) last_played);
        return false;
    }


    private boolean canPlay(ColoredCard last_played) {
        if (this.getColor() == last_played.getColor()) return true;
        if (this.getValue() == last_played.getValue()) return true;

        return false;
    }

    private boolean canPlay(NeutralCard last_played) {
        return last_played.getPickedColor() == this.getColor();
    }


    @Override
    public CardValue getValue() {
        return this.value;
    }

    public Color getColor() {
        return this.color;
    }
}
