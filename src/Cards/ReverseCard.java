package Cards;

public class ReverseCard extends ColoredCard {

    public ReverseCard(Color color) {
        super(color, CardValue.REVERSE);
    }
}
