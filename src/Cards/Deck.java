package Cards;

import java.util.ArrayList;
import java.util.Collections;

public class Deck {

    private ArrayList<Card> cards = new ArrayList<Card>();

    public Deck() {
        for (int c = 0; c < Color.values().length; c++) {
            Color color = Color.values()[c];
            addToArray(cards, color);
        }

        for (int i = 0; i < 4; i++){
            cards.add(new WildCard());
            cards.add(new PickUpFour());
        }
    }

    private void addToArray(ArrayList<Card> array, Color color) {
        for (int i = 0; i < 2; i++ ) {
            array.add(new NumberedCard(color, CardValue.ZERO));
            array.add(new NumberedCard(color, CardValue.ONE));
            array.add(new NumberedCard(color, CardValue.TWO));
            array.add(new NumberedCard(color, CardValue.THREE));
            array.add(new NumberedCard(color, CardValue.FOUR));
            array.add(new NumberedCard(color, CardValue.FIVE));
            array.add(new NumberedCard(color, CardValue.SIX));
            array.add(new NumberedCard(color, CardValue.SEVEN));
            array.add(new NumberedCard(color, CardValue.EIGHT));
            array.add(new NumberedCard(color, CardValue.NINE));
            array.add(new PickUpTwo(color));
            array.add(new ReverseCard(color));
            array.add(new SkipCard(color));
        }
    }
    public void addToDeck(Card card) {
       this.cards.add(card) ;
    }

    public Card draw() {
        return this.cards.remove(0);
    }

    public void shuffle() {
        Collections.shuffle(this.cards);
    }

}
