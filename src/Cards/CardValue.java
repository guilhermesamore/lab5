package Cards;

public enum CardValue {
    ZERO,
    ONE,
    TWO,
    THREE,
    FOUR,
    FIVE,
    SIX,
    SEVEN,
    EIGHT,
    NINE,
    SKIP,
    REVERSE,
    PICKUP_TWO,
    WILDCARD,
    PICKUP_FOUR
}
