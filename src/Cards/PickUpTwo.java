package Cards;

public class PickUpTwo extends ColoredCard {
    public PickUpTwo(Color color) {
        super(color, CardValue.PICKUP_TWO);
    }
}
