package Cards;

public class NumberedCard extends ColoredCard {

    public NumberedCard(Color color, CardValue value) {
        super(color, value);

        if (value.ordinal() > 9) {
            throw new IllegalArgumentException("The numbered card should have a number value.");
        }

    }
}
