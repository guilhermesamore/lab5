package Cards;

public abstract class NeutralCard implements Card {
    private final CardValue cardValue;
    private Color pickedColor;

    public NeutralCard(CardValue cardValue) {
       this.cardValue = cardValue;
    }

    @Override
    public CardValue getValue() {
        return this.cardValue;
    }

    @Override
    public boolean canPlay(Card card) {
        return true;
    }

    public void pickColor(Color color) {
        this.pickedColor = color;
    }

    public Color getPickedColor() {
        return this.pickedColor;
    }


}
