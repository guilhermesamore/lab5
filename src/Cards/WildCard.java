package Cards;

public class WildCard extends NeutralCard {
    public WildCard() {
        super(CardValue.WILDCARD);
    }
}
