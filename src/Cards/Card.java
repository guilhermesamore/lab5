package Cards;

public interface Card {

    boolean canPlay(Card last_played);
    CardValue getValue();

}
