package Cards;

public class SkipCard extends ColoredCard {
    public SkipCard(Color color) {
        super(color, CardValue.SKIP);
    }
}
